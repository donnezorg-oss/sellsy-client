<?php

namespace DonnezOrg\SellsyClient\Entity\Address;

use DonnezOrg\SellsyClient\Entity\Address\GeoCode\GeoCode;
use Symfony\Component\Validator\Constraints as Assert;

abstract class AbstractAddress
{
    protected int $id;

    #[Assert\Length(min: 2)]
    protected string $name;

    protected ?string $addressLine1;
    protected ?string $addressLine2;
    protected ?string $addressLine3;
    protected ?string $addressLine4;

    #[Assert\Length(min: 2)]
    protected ?string $postalCode;

    #[Assert\Length(min: 2)]
    protected string $city;

    #[Assert\Length(min: 2)]
    protected string $country;

    #[Assert\Length(exactly: 2)]
    protected string $countryCode;
    protected bool $isInvoicingAddress;
    protected bool $isDeliveryAddress;
    protected GeoCode $geocode;
}
