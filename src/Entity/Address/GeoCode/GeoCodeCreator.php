<?php

namespace DonnezOrg\SellsyClient\Entity\Address\GeoCode;

use DonnezOrg\SellsyClient\Entity\EntityCreator;

class GeoCodeCreator extends AbstractGeoCode implements EntityCreator
{
    public function __construct(float $lat = null, float $lng = null)
    {
        $this->lat = $lat;
        $this->lng = $lng;
    }

    public function getLat(): ?float
    {
        return $this->lat;
    }

    public function getLng(): ?float
    {
        return $this->lng;
    }

    public function toGeoCode(): GeoCode
    {
        return (new GeoCode())
            ->setLat($this->lat)
            ->setLng($this->lng);
    }
}
