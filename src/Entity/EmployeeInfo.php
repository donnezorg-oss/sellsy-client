<?php

namespace DonnezOrg\SellsyClient\Entity;

class EmployeeInfo
{
    private int $id;
    private string $label;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): EmployeeInfo
    {
        $this->id = $id;

        return $this;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function setLabel(string $label): EmployeeInfo
    {
        $this->label = $label;

        return $this;
    }
}
