<?php

namespace DonnezOrg\SellsyClient\Entity\Opportunity;

final class Pipeline
{
    private int $id;
    private string $name;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): Pipeline
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Pipeline
    {
        $this->name = $name;

        return $this;
    }
}
