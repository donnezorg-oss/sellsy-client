<?php

namespace DonnezOrg\SellsyClient\Entity\Opportunity;

use DonnezOrg\SellsyClient\Entity\EntityMutator;
use DonnezOrg\SellsyClient\Entity\Enum\OpportunityStatus;

class OpportunityMutator extends AbstractOpportunity implements EntityMutator
{
    private string $amount;
    private int $pipeline;
    private int $step;
    private int $ownerId;
    private int $source;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): OpportunityMutator
    {
        $this->name = $name;

        return $this;
    }

    public function getDueDate(): ?\DateTime
    {
        return $this->dueDate;
    }

    public function setDueDate(?\DateTime $dueDate): OpportunityMutator
    {
        $this->dueDate = $dueDate;

        return $this;
    }

    public function getStatus(): OpportunityStatus
    {
        return $this->status;
    }

    public function setStatus(OpportunityStatus $status): OpportunityMutator
    {
        $this->status = $status;

        return $this;
    }

    public function getAmount(): string
    {
        return $this->amount;
    }

    public function setAmount(string $amount): OpportunityMutator
    {
        $this->amount = $amount;

        return $this;
    }

    public function getProbability(): int
    {
        return $this->probability;
    }

    public function setProbability(int $probability): OpportunityMutator
    {
        $this->probability = $probability;

        return $this;
    }

    public function getNote(): string
    {
        return $this->note;
    }

    public function setNote(string $note): OpportunityMutator
    {
        $this->note = $note;

        return $this;
    }

    public function getAssignedStaffIds(): ?array
    {
        return $this->assignedStaffIds;
    }

    public function setAssignedStaffIds(?array $assignedStaffIds): OpportunityMutator
    {
        $this->assignedStaffIds = $assignedStaffIds;

        return $this;
    }

    public function getContactIds(): ?array
    {
        return $this->contactIds;
    }

    public function setContactIds(?array $contactIds): OpportunityMutator
    {
        $this->contactIds = $contactIds;

        return $this;
    }

    public function getMainDocId(): ?int
    {
        return $this->mainDocId;
    }

    public function setMainDocId(?int $mainDocId): OpportunityMutator
    {
        $this->mainDocId = $mainDocId;

        return $this;
    }

    public function getRelated(): array
    {
        return $this->related;
    }

    public function setRelated(array $related): OpportunityMutator
    {
        $this->related = $related;

        return $this;
    }

    public function getPipeline(): int
    {
        return $this->pipeline;
    }

    public function setPipeline(int $pipeline): OpportunityMutator
    {
        $this->pipeline = $pipeline;

        return $this;
    }

    public function getStep(): int
    {
        return $this->step;
    }

    public function setStep(int $step): OpportunityMutator
    {
        $this->step = $step;

        return $this;
    }

    public function getOwnerId(): int
    {
        return $this->ownerId;
    }

    public function setOwnerId(int $ownerId): OpportunityMutator
    {
        $this->ownerId = $ownerId;

        return $this;
    }

    public function getSource(): int
    {
        return $this->source;
    }

    public function setSource(int $source): OpportunityMutator
    {
        $this->source = $source;

        return $this;
    }
}
