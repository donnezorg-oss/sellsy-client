<?php

namespace DonnezOrg\SellsyClient\Entity\Opportunity;

use DonnezOrg\SellsyClient\Entity\Enum\OpportunityStatus;
use DonnezOrg\SellsyClient\Entity\Owner;

class Opportunity extends AbstractOpportunity
{
    protected Amount $amount;
    protected Pipeline $pipeline;
    protected Step $step;
    protected Owner $owner;
    protected Source $source;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): Opportunity
    {
        $this->id = $id;

        return $this;
    }

    public function getNumber(): string
    {
        return $this->number;
    }

    public function setNumber(string $number): Opportunity
    {
        $this->number = $number;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Opportunity
    {
        $this->name = $name;

        return $this;
    }

    public function getSource(): Source
    {
        return $this->source;
    }

    public function setSource(Source $source): Opportunity
    {
        $this->source = $source;

        return $this;
    }

    public function getDueDate(): ?\DateTime
    {
        return $this->dueDate;
    }

    public function setDueDate(?\DateTime $dueDate): Opportunity
    {
        $this->dueDate = $dueDate;

        return $this;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    public function setCreated(\DateTime $created): Opportunity
    {
        $this->created = $created;

        return $this;
    }

    public function getUpdatedStatus(): ?\DateTime
    {
        return $this->updatedStatus;
    }

    public function setUpdatedStatus(?\DateTime $updatedStatus): Opportunity
    {
        $this->updatedStatus = $updatedStatus;

        return $this;
    }

    public function getStatus(): OpportunityStatus
    {
        return $this->status;
    }

    public function setStatus(OpportunityStatus $status): Opportunity
    {
        $this->status = $status;

        return $this;
    }

    public function getAmount(): Amount
    {
        return $this->amount;
    }

    public function setAmount(Amount $amount): Opportunity
    {
        $this->amount = $amount;

        return $this;
    }

    public function getPipeline(): Pipeline
    {
        return $this->pipeline;
    }

    public function setPipeline(Pipeline $pipeline): Opportunity
    {
        $this->pipeline = $pipeline;

        return $this;
    }

    public function getStep(): Step
    {
        return $this->step;
    }

    public function setStep(Step $step): Opportunity
    {
        $this->step = $step;

        return $this;
    }

    public function getProbability(): int
    {
        return $this->probability;
    }

    public function setProbability(int $probability): Opportunity
    {
        $this->probability = $probability;

        return $this;
    }

    public function getNote(): string
    {
        return $this->note;
    }

    public function setNote(string $note): Opportunity
    {
        $this->note = $note;

        return $this;
    }

    public function getOwner(): Owner
    {
        return $this->owner;
    }

    public function setOwner(Owner $owner): Opportunity
    {
        $this->owner = $owner;

        return $this;
    }

    public function getAssignedStaffIds(): ?array
    {
        return $this->assignedStaffIds;
    }

    public function setAssignedStaffIds(?array $assignedStaffIds): Opportunity
    {
        $this->assignedStaffIds = $assignedStaffIds;

        return $this;
    }

    public function getContactIds(): ?array
    {
        return $this->contactIds;
    }

    public function setContactIds(?array $contactIds): Opportunity
    {
        $this->contactIds = $contactIds;

        return $this;
    }

    public function getCompanyId(): ?int
    {
        return $this->companyId;
    }

    public function setCompanyId(?int $companyId): Opportunity
    {
        $this->companyId = $companyId;

        return $this;
    }

    public function getIndividualId(): ?int
    {
        return $this->individualId;
    }

    public function setIndividualId(?int $individualId): Opportunity
    {
        $this->individualId = $individualId;

        return $this;
    }

    public function getMainDocId(): ?int
    {
        return $this->mainDocId;
    }

    public function setMainDocId(?int $mainDocId): Opportunity
    {
        $this->mainDocId = $mainDocId;

        return $this;
    }

    public function getRelated(): array
    {
        return $this->related;
    }

    public function setRelated(array $related): Opportunity
    {
        $this->related = $related;

        return $this;
    }
}
