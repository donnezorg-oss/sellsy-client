<?php

namespace DonnezOrg\SellsyClient\Entity\Opportunity;

use DonnezOrg\SellsyClient\Entity\EntityMutator;

class OpportunityRankAndStepMutator implements EntityMutator
{
    private int $step;
    private ?int $beforeSibling;

    public function __construct(int $step)
    {
        $this->step = $step;
    }

    public function getStep(): int
    {
        return $this->step;
    }

    public function setStep(int $step): OpportunityRankAndStepMutator
    {
        $this->step = $step;

        return $this;
    }

    public function getBeforeSibling(): ?int
    {
        return $this->beforeSibling;
    }

    public function setBeforeSibling(?int $beforeSibling): OpportunityRankAndStepMutator
    {
        $this->beforeSibling = $beforeSibling;

        return $this;
    }
}
