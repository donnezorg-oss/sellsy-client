<?php

namespace DonnezOrg\SellsyClient\Entity\Opportunity;

use DonnezOrg\SellsyClient\Entity\{EntityCreator, Enum\OpportunityStatus, Relation};

final class OpportunityCreator extends AbstractOpportunity implements EntityCreator
{
    private string $amount;
    private int $pipeline;
    private int $step;
    private int $ownerId;
    private int $source;

    /**
     * @param array<Relation> $related
     */
    public function __construct(string $name, int $pipeline, int $step, array $related)
    {
        $this->name = $name;
        $this->pipeline = $pipeline;
        $this->step = $step;
        $this->related = $related;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): OpportunityCreator
    {
        $this->name = $name;

        return $this;
    }

    public function getOwnerId(): int
    {
        return $this->ownerId;
    }

    public function setOwnerId(int $ownerId): OpportunityCreator
    {
        $this->ownerId = $ownerId;

        return $this;
    }

    public function getStatus(): OpportunityStatus
    {
        return $this->status;
    }

    public function setStatus(OpportunityStatus $status): OpportunityCreator
    {
        $this->status = $status;

        return $this;
    }

    public function getPipeline(): int
    {
        return $this->pipeline;
    }

    public function setPipeline(int $pipeline): OpportunityCreator
    {
        $this->pipeline = $pipeline;

        return $this;
    }

    public function getStep(): int
    {
        return $this->step;
    }

    public function setStep(int $step): OpportunityCreator
    {
        $this->step = $step;

        return $this;
    }

    public function getAmount(): string
    {
        return $this->amount;
    }

    public function setAmount(string $amount): OpportunityCreator
    {
        $this->amount = $amount;

        return $this;
    }

    public function getProbability(): int
    {
        return $this->probability;
    }

    public function setProbability(int $probability): OpportunityCreator
    {
        $this->probability = $probability;

        return $this;
    }

    public function getSource(): int
    {
        return $this->source;
    }

    public function setSource(int $source): OpportunityCreator
    {
        $this->source = $source;

        return $this;
    }

    public function getDueDate(): ?\DateTime
    {
        return $this->dueDate;
    }

    public function setDueDate(?\DateTime $dueDate): OpportunityCreator
    {
        $this->dueDate = $dueDate;

        return $this;
    }

    public function getNote(): string
    {
        return $this->note;
    }

    public function setNote(string $note): OpportunityCreator
    {
        $this->note = $note;

        return $this;
    }

    public function getMainDocId(): ?int
    {
        return $this->mainDocId;
    }

    public function setMainDocId(?int $mainDocId): OpportunityCreator
    {
        $this->mainDocId = $mainDocId;

        return $this;
    }

    public function getAssignedStaffIds(): ?array
    {
        return $this->assignedStaffIds;
    }

    public function setAssignedStaffIds(?array $assignedStaffIds): OpportunityCreator
    {
        $this->assignedStaffIds = $assignedStaffIds;

        return $this;
    }

    public function getContactIds(): ?array
    {
        return $this->contactIds;
    }

    public function setContactIds(?array $contactIds): OpportunityCreator
    {
        $this->contactIds = $contactIds;

        return $this;
    }

    public function getRelated(): array
    {
        return $this->related;
    }

    public function setRelated(array $related): OpportunityCreator
    {
        $this->related = $related;

        return $this;
    }

    public function getNumber(): string
    {
        return $this->number;
    }

    public function setNumber(string $number): OpportunityCreator
    {
        $this->number = $number;

        return $this;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    public function setCreated(\DateTime $created): OpportunityCreator
    {
        $this->created = $created;

        return $this;
    }
}
