<?php

namespace DonnezOrg\SellsyClient\Entity\Opportunity;

use DonnezOrg\SellsyClient\Entity\{DateFilter, Enum\OpportunityStatus, Range, Relation};

class SearchFilter
{
    private DateFilter $created;
    private DateFilter $updatedStatus;
    private DateFilter $dueStatus;
    private string $name;

    /**
     * @var array<int>
     */
    private array $pipeline;

    /** @var array<int> */
    private array $step;

    private Range $amount;

    /**
     * @var array<Relation>
     */
    private array $relatedObjects;

    /**
     * @var array<int>
     */
    private array $assignedStaffs;

    /**
     * @var array<OpportunityStatus>
     */
    private array $statuses;

    private int $favoriteFilter;

    public function getCreated(): DateFilter
    {
        return $this->created;
    }

    public function setCreated(DateFilter $created): SearchFilter
    {
        $this->created = $created;

        return $this;
    }

    public function getUpdatedStatus(): DateFilter
    {
        return $this->updatedStatus;
    }

    public function setUpdatedStatus(DateFilter $updatedStatus): SearchFilter
    {
        $this->updatedStatus = $updatedStatus;

        return $this;
    }

    public function getDueStatus(): DateFilter
    {
        return $this->dueStatus;
    }

    public function setDueStatus(DateFilter $dueStatus): SearchFilter
    {
        $this->dueStatus = $dueStatus;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): SearchFilter
    {
        $this->name = $name;

        return $this;
    }

    public function getPipeline(): array
    {
        return $this->pipeline;
    }

    public function setPipeline(array $pipeline): SearchFilter
    {
        $this->pipeline = $pipeline;

        return $this;
    }

    public function getStep(): array
    {
        return $this->step;
    }

    public function setStep(array $step): SearchFilter
    {
        $this->step = $step;

        return $this;
    }

    public function getAmount(): Range
    {
        return $this->amount;
    }

    public function setAmount(Range $amount): SearchFilter
    {
        $this->amount = $amount;

        return $this;
    }

    public function getRelatedObjects(): array
    {
        return $this->relatedObjects;
    }

    public function setRelatedObjects(array $relatedObjects): SearchFilter
    {
        $this->relatedObjects = $relatedObjects;

        return $this;
    }

    public function getAssignedStaffs(): array
    {
        return $this->assignedStaffs;
    }

    public function setAssignedStaffs(array $assignedStaffs): SearchFilter
    {
        $this->assignedStaffs = $assignedStaffs;

        return $this;
    }

    public function getStatuses(): array
    {
        return $this->statuses;
    }

    public function setStatuses(array $statuses): SearchFilter
    {
        $this->statuses = $statuses;

        return $this;
    }

    public function getFavoriteFilter(): int
    {
        return $this->favoriteFilter;
    }

    public function setFavoriteFilter(int $favoriteFilter): SearchFilter
    {
        $this->favoriteFilter = $favoriteFilter;

        return $this;
    }
}
