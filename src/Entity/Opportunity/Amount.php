<?php

namespace DonnezOrg\SellsyClient\Entity\Opportunity;

final class Amount
{
    private string|float $value;
    private string $currency;

    public function getValue(): string|float
    {
        return $this->value;
    }

    public function setValue(string|float $value): Amount
    {
        $this->value = $value;

        return $this;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency In format ISO 4217 (e.g. EUR)
     */
    public function setCurrency(string $currency): Amount
    {
        $this->currency = $currency;

        return $this;
    }
}
