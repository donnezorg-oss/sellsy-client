<?php

namespace DonnezOrg\SellsyClient\Entity\Opportunity;

final class Source
{
    private int $id;
    private string $name;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): Source
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Source
    {
        $this->name = $name;

        return $this;
    }
}
