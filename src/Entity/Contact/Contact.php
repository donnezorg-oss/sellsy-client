<?php

namespace DonnezOrg\SellsyClient\Entity\Contact;

use DonnezOrg\SellsyClient\Entity\{
    Enum\ContactCivility,
    Enum\MarketingCampaignSubscription,
    Social,
    Sync
};

final class Contact extends AbstractContact
{
    /**
     * Get the value of id.
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set the value of id.
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of civility.
     */
    public function getCivility(): ?ContactCivility
    {
        return $this->civility;
    }

    /**
     * Set the value of civility.
     */
    public function setCivility(?ContactCivility $civility): self
    {
        $this->civility = $civility;

        return $this;
    }

    /**
     * Get the value of firstName.
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * Set the value of firstName.
     */
    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get the value of lastName.
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * Set the value of lastName.
     */
    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get the value of email.
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * Set the value of email.
     */
    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of website.
     */
    public function getWebsite(): ?string
    {
        return $this->website;
    }

    /**
     * Set the value of website.
     */
    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get the value of phoneNumber.
     */
    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    /**
     * Set the value of phoneNumber.
     */
    public function setPhoneNumber(?string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get the value of mobileNumber.
     */
    public function getMobileNumber(): ?string
    {
        return $this->mobileNumber;
    }

    /**
     * Set the value of mobileNumber.
     */
    public function setMobileNumber(?string $mobileNumber): self
    {
        $this->mobileNumber = $mobileNumber;

        return $this;
    }

    /**
     * Get the value of faxNumber.
     */
    public function getFaxNumber(): ?string
    {
        return $this->faxNumber;
    }

    /**
     * Set the value of faxNumber.
     */
    public function setFaxNumber(?string $faxNumber): self
    {
        $this->faxNumber = $faxNumber;

        return $this;
    }

    /**
     * Get the value of position.
     */
    public function getPosition(): ?string
    {
        return $this->position;
    }

    /**
     * Set the value of position.
     */
    public function setPosition(?string $position): self
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get the value of birthDate.
     */
    public function getBirthDate(): ?string
    {
        return $this->birthDate;
    }

    /**
     * Set the value of birthDate.
     */
    public function setBirthDate(?string $birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * Get the value of avatar.
     */
    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    /**
     * Set the value of avatar.
     */
    public function setAvatar(?string $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get the value of note.
     */
    public function getNote(): ?string
    {
        return $this->note;
    }

    /**
     * Set the value of note.
     */
    public function setNote(?string $note): self
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get the value of social.
     */
    public function getSocial(): ?Social
    {
        return $this->social;
    }

    /**
     * Set the value of social.
     */
    public function setSocial(?Social $social): self
    {
        $this->social = $social;

        return $this;
    }

    /**
     * Get the value of sync.
     */
    public function getSync(): ?Sync
    {
        return $this->sync;
    }

    /**
     * Set the value of sync.
     */
    public function setSync(?Sync $sync): self
    {
        $this->sync = $sync;

        return $this;
    }

    /**
     * Get the value of created.
     */
    public function getCreated(): ?\DateTime
    {
        return $this->created;
    }

    /**
     * Set the value of created.
     */
    public function setCreated(?\DateTime $created): self
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get the value of isArchived.
     */
    public function getIsArchived(): ?bool
    {
        return $this->isArchived;
    }

    /**
     * Set the value of isArchived.
     */
    public function setIsArchived(?bool $isArchived): self
    {
        $this->isArchived = $isArchived;

        return $this;
    }

    /**
     * Get the value of ownerId.
     */
    public function getOwnerId(): ?int
    {
        return $this->ownerId;
    }

    /**
     * Set the value of ownerId.
     */
    public function setOwnerId(?int $ownerId): self
    {
        $this->ownerId = $ownerId;

        return $this;
    }

    /**
     * @return array<\DonnezOrg\SellsyClient\Entity\Enum\MarketingCampaignSubscription>
     *
     * @noinspection PhpUnnecessaryFullyQualifiedNameInspection
     *
     * @psalm-suppress RedundantPropertyInitializationCheck
     */
    public function getMarketingCampaignsSubscriptions(): array
    {
        return $this->marketingCampaignsSubscriptions ?? [];
    }

    /**
     * @param array<\DonnezOrg\SellsyClient\Entity\Enum\MarketingCampaignSubscription> $marketingCampaignsSubscriptions
     *
     * @noinspection PhpUnnecessaryFullyQualifiedNameInspection
     */
    public function setMarketingCampaignsSubscriptions(array $marketingCampaignsSubscriptions): self
    {
        $this->marketingCampaignsSubscriptions = $marketingCampaignsSubscriptions;

        return $this;
    }

    /**
     * @return $this
     *
     * @psalm-suppress RedundantPropertyInitializationCheck
     */
    public function addMarketingCampaignsSubscription(MarketingCampaignSubscription $subscription): self
    {
        if (!isset($this->marketingCampaignsSubscriptions)) {
            $this->marketingCampaignsSubscriptions = [];
        }

        $this->marketingCampaignsSubscriptions[] = $subscription;

        return $this;
    }

    /**
     * @return $this
     *
     * @psalm-suppress RedundantPropertyInitializationCheck
     */
    public function removeMarketingCampaignsSubscription(MarketingCampaignSubscription $subscription): self
    {
        if (!isset($this->marketingCampaignsSubscriptions)) {
            return $this;
        }

        $index = array_search($subscription, $this->marketingCampaignsSubscriptions);
        if (false !== $index) {
            array_splice($this->marketingCampaignsSubscriptions, $index, 1);
        }

        return $this;
    }
}
