<?php

namespace DonnezOrg\SellsyClient\Entity\Contact;

class ContactLink
{
    /**
     * @var array<string>|null
     */
    protected ?array $roles;

    /**
     * @return array<string>|null
     */
    public function getRoles(): ?array
    {
        return $this->roles;
    }
}
