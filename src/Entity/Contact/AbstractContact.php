<?php

namespace DonnezOrg\SellsyClient\Entity\Contact;

use DonnezOrg\SellsyClient\Entity\{
    Enum\ContactCivility,
    Social,
    Sync
};
use Symfony\Component\Validator\Constraints as Assert;

abstract class AbstractContact
{
    protected int $id;
    protected ?ContactCivility $civility;
    protected ?string $firstName;
    protected string $lastName;

    #[Assert\Email(mode: 'html5')]
    protected ?string $email;

    #[Assert\Url]
    protected ?string $website;

    protected ?string $phoneNumber;
    protected ?string $mobileNumber;
    protected ?string $faxNumber;
    protected ?string $position;
    protected ?string $birthDate;
    protected ?string $avatar;
    protected ?string $note;
    protected ?Social $social;
    protected ?Sync $sync;
    protected ?\DateTime $created;
    protected ?bool $isArchived;
    protected ?int $ownerId;

    /**
     * @var array<\DonnezOrg\SellsyClient\Entity\Enum\MarketingCampaignSubscription>
     *
     * @noinspection PhpFullyQualifiedNameUsageInspection
     */
    protected array $marketingCampaignsSubscriptions;

    public function __construct()
    {
    }
}
