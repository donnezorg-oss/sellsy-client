<?php

namespace DonnezOrg\SellsyClient\Entity\SmartTag;

use DonnezOrg\SellsyClient\Entity\EntityCreator;

class SmartTagLinkByValueCreator extends AbstractSmartTag implements EntityCreator
{
    public function __construct(string $value)
    {
        $this->value = $value;
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
