<?php

namespace DonnezOrg\SellsyClient\Entity\SmartTag;

abstract class AbstractSmartTag
{
    protected int $id;
    protected string $value;
}
