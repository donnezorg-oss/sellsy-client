<?php

namespace DonnezOrg\SellsyClient\Entity\SmartTag;

use DonnezOrg\SellsyClient\Entity\EntityCreator;

class SmartTagLinkByIdentifierCreator extends AbstractSmartTag implements EntityCreator
{
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }
}
