<?php

namespace DonnezOrg\SellsyClient\Entity\SmartTag;

class SmartTag extends AbstractSmartTag
{
    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function setValue(string $value): void
    {
        $this->value = $value;
    }
}
