<?php

namespace DonnezOrg\SellsyClient\Entity;

final class Sync
{
    private ?bool $mailchimp;
    private ?bool $mailjet;
    private ?bool $simplemail;

    public function getMailchimp(): ?bool
    {
        return $this->mailchimp;
    }

    public function setMailchimp(?bool $mailchimp): self
    {
        $this->mailchimp = $mailchimp;

        return $this;
    }

    public function getMailjet(): ?bool
    {
        return $this->mailjet;
    }

    public function setMailjet(?bool $mailjet): self
    {
        $this->mailjet = $mailjet;

        return $this;
    }

    public function getSimplemail(): ?bool
    {
        return $this->simplemail;
    }

    public function setSimplemail(?bool $simplemail): self
    {
        $this->simplemail = $simplemail;

        return $this;
    }
}
