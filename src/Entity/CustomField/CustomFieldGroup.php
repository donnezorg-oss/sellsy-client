<?php

namespace DonnezOrg\SellsyClient\Entity\CustomField;

use Symfony\Component\Validator\Constraints as Assert;

class CustomFieldGroup
{
    private int $id;

    #[Assert\Length(max: 250)]
    private string $name;

    #[Assert\Length(max: 250)]
    private string $code;

    private bool $openByDefault;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function isOpenByDefault(): bool
    {
        return $this->openByDefault;
    }

    public function setOpenByDefault(bool $openByDefault): self
    {
        $this->openByDefault = $openByDefault;

        return $this;
    }
}
