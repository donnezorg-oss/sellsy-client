<?php

namespace DonnezOrg\SellsyClient\Entity\CustomField;

class UnhandledCustomField extends AbstractCustomField
{
    public function __construct(int $id, string $name, private readonly string $type)
    {
        parent::setId($id)
            ->setName($name)
            ->setDescription('The type of this custom field is not handled by the sellsy client');
    }

    public function getType(): string
    {
        return $this->type;
    }
}
