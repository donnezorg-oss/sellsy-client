<?php

namespace DonnezOrg\SellsyClient\Entity\CustomField;

use DonnezOrg\SellsyClient\Entity\CustomField\Parameter\SimpleTextParameter;

class SimpleTextCustomField extends AbstractCustomField
{
    public function getType(): string
    {
        return 'simple-text';
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): void
    {
        $this->value = $value;
    }

    public function getParameters(): SimpleTextParameter
    {
        return $this->parameters;
    }

    public function setParameters(SimpleTextParameter $parameters): void
    {
        $this->parameters = $parameters;
    }
}
