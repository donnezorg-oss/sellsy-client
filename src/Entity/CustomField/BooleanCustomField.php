<?php

namespace DonnezOrg\SellsyClient\Entity\CustomField;

use DonnezOrg\SellsyClient\Entity\CustomField\Parameter\BooleanParameter;

class BooleanCustomField extends AbstractCustomField
{
    public function getType(): string
    {
        return 'boolean';
    }

    public function getValue(): ?bool
    {
        return $this->value;
    }

    public function setValue(?bool $value): void
    {
        $this->value = $value;
    }

    public function getParameters(): BooleanParameter
    {
        return $this->parameters;
    }

    public function setParameters(BooleanParameter $parameters): void
    {
        $this->parameters = $parameters;
    }
}
