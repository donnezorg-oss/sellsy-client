<?php

namespace DonnezOrg\SellsyClient\Entity\CustomField\Parameter;

class BooleanParameter
{
    private ?bool $defaultValue;

    public function getDefaultValue(): ?bool
    {
        return $this->defaultValue;
    }

    public function setDefaultValue(?bool $defaultValue): self
    {
        $this->defaultValue = $defaultValue;

        return $this;
    }
}
