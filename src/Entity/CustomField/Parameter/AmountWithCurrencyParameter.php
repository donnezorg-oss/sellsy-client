<?php

namespace DonnezOrg\SellsyClient\Entity\CustomField\Parameter;

use DonnezOrg\SellsyClient\Entity\Enum\CurrencyType;

class AmountWithCurrencyParameter
{
    private ?string $defaultValue;
    private ?string $minValue;
    private ?string $maxValue;
    private ?CurrencyType $currency;

    public function getDefaultValue(): ?string
    {
        return $this->defaultValue;
    }

    public function setDefaultValue(?string $defaultValue): self
    {
        $this->defaultValue = $defaultValue;

        return $this;
    }

    public function getMinValue(): ?string
    {
        return $this->minValue;
    }

    public function setMinValue(?string $minValue): self
    {
        $this->minValue = $minValue;

        return $this;
    }

    public function getMaxValue(): ?string
    {
        return $this->maxValue;
    }

    public function setMaxValue(?string $maxValue): self
    {
        $this->maxValue = $maxValue;

        return $this;
    }

    public function getUnit(): ?CurrencyType
    {
        return $this->currency;
    }

    public function setUnit(?CurrencyType $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getCurrency(): ?CurrencyType
    {
        return $this->currency;
    }

    public function setCurrency(?CurrencyType $currency): self
    {
        $this->currency = $currency;

        return $this;
    }
}
