<?php

namespace DonnezOrg\SellsyClient\Entity\CustomField\Parameter;

use DonnezOrg\SellsyClient\Entity\Enum\UnitType;

class NumericWithUnitParameter
{
    private ?string $defaultValue;
    private ?string $minValue;
    private ?string $maxValue;
    private ?UnitType $unit;

    public function getDefaultValue(): ?string
    {
        return $this->defaultValue;
    }

    public function setDefaultValue(?string $defaultValue): self
    {
        $this->defaultValue = $defaultValue;

        return $this;
    }

    public function getMinValue(): ?string
    {
        return $this->minValue;
    }

    public function setMinValue(?string $minValue): self
    {
        $this->minValue = $minValue;

        return $this;
    }

    public function getMaxValue(): ?string
    {
        return $this->maxValue;
    }

    public function setMaxValue(?string $maxValue): self
    {
        $this->maxValue = $maxValue;

        return $this;
    }

    public function getUnit(): ?UnitType
    {
        return $this->unit;
    }

    public function setUnit(?UnitType $unit): self
    {
        $this->unit = $unit;

        return $this;
    }
}
