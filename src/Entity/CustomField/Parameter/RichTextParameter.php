<?php

namespace DonnezOrg\SellsyClient\Entity\CustomField\Parameter;

use Symfony\Component\Validator\Constraints as Assert;

class RichTextParameter
{
    #[Assert\Length(min: 0, max: 65_000)]
    private ?string $defaultValue;

    #[Assert\Range(min: 0, max: 65_000)]
    private ?int $charMin;

    #[Assert\Range(min: 0, max: 65_000)]
    private ?int $charMax;

    public function getDefaultValue(): ?string
    {
        return $this->defaultValue;
    }

    public function setDefaultValue(?string $defaultValue): self
    {
        $this->defaultValue = $defaultValue;

        return $this;
    }

    public function getCharMin(): ?int
    {
        return $this->charMin;
    }

    public function setCharMin(?int $charMin): self
    {
        $this->charMin = $charMin;

        return $this;
    }

    public function getCharMax(): ?int
    {
        return $this->charMax;
    }

    public function setCharMax(?int $charMax): self
    {
        $this->charMax = $charMax;

        return $this;
    }
}
