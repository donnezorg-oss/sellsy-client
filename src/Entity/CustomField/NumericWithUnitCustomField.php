<?php

namespace DonnezOrg\SellsyClient\Entity\CustomField;

use DonnezOrg\SellsyClient\Entity\CustomField\Parameter\NumericWithUnitParameter;

class NumericWithUnitCustomField extends AbstractCustomField
{
    public function getType(): string
    {
        return 'numeric-with-unit';
    }

    public function getValue(): ValueWithUnit
    {
        return $this->value;
    }

    public function setValue(ValueWithUnit $value): void
    {
        $this->value = $value;
    }

    public function getParameters(): NumericWithUnitParameter
    {
        return $this->parameters;
    }

    public function setParameters(NumericWithUnitParameter $parameters): void
    {
        $this->parameters = $parameters;
    }
}

class ValueWithUnit
{
    private ?string $number;
    private ?int $unitId;

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(?string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getUnitId(): ?int
    {
        return $this->unitId;
    }

    public function setUnitId(?int $unitId): self
    {
        $this->unitId = $unitId;

        return $this;
    }
}
