<?php

namespace DonnezOrg\SellsyClient\Entity\Enum;

enum CompanyType: string
{
    case PROSPECT = 'prospect';
    case CLIENT = 'client';
    case SUPPLIER = 'supplier';
}
