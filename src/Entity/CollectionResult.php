<?php

namespace DonnezOrg\SellsyClient\Entity;

/**
 * @template T
 */
class CollectionResult
{
    /**
     * @var array<T>
     */
    private array $data = [];

    private Pagination $pagination;

    /**
     * @return array<T>
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array<T> $data
     *
     * @return CollectionResult<T>
     */
    public function setData(array $data): CollectionResult
    {
        $this->data = $data;

        return $this;
    }

    public function getPagination(): Pagination
    {
        return $this->pagination;
    }

    public function setPagination(Pagination $pagination): void
    {
        $this->pagination = $pagination;
    }
}
