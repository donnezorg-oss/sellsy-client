<?php

namespace DonnezOrg\SellsyClient\Entity\Company;

use DonnezOrg\SellsyClient\Entity\Enum\CompanyType;
use DonnezOrg\SellsyClient\Entity\{
    BusinessSegmentInfo,
    EmployeeInfo,
    Enum\MarketingCampaignSubscription,
    LegalFrance,
    Owner,
    Social
};

final class Company extends AbstractCompany
{
    protected ?EmployeeInfo $numberOfEmployees;
    protected ?BusinessSegmentInfo $businessSegment;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @noinspection PhpUnnecessaryFullyQualifiedNameInspection
     */
    public function getType(): CompanyType
    {
        return $this->type;
    }

    /**
     * @noinspection PhpUnnecessaryFullyQualifiedNameInspection
     */
    public function setType(CompanyType $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getMobileNumber(): ?string
    {
        return $this->mobileNumber;
    }

    public function setMobileNumber(?string $mobileNumber): self
    {
        $this->mobileNumber = $mobileNumber;

        return $this;
    }

    public function getFaxNumber(): ?string
    {
        return $this->faxNumber;
    }

    public function setFaxNumber(?string $faxNumber): self
    {
        $this->faxNumber = $faxNumber;

        return $this;
    }

    public function getLegalFrance(): ?LegalFrance
    {
        return $this->legalFrance;
    }

    public function setLegalFrance(?LegalFrance $legalFrance): self
    {
        $this->legalFrance = $legalFrance;

        return $this;
    }

    public function getCapital(): ?string
    {
        return $this->capital;
    }

    public function setCapital(?string $capital): self
    {
        $this->capital = $capital;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(?string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getNote(): string
    {
        return $this->note;
    }

    public function setNote(string $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getAuxiliaryCode(): ?string
    {
        return $this->auxiliaryCode;
    }

    public function setAuxiliaryCode(?string $auxiliaryCode): self
    {
        $this->auxiliaryCode = $auxiliaryCode;

        return $this;
    }

    public function getSocial(): ?Social
    {
        return $this->social;
    }

    public function setSocial(?Social $social): self
    {
        $this->social = $social;

        return $this;
    }

    public function getRateCategoryId(): ?int
    {
        return $this->rateCategoryId;
    }

    public function setRateCategoryId(?int $rateCategoryId): self
    {
        $this->rateCategoryId = $rateCategoryId;

        return $this;
    }

    public function getAccountingCodeId(): ?int
    {
        return $this->accountingCodeId;
    }

    public function setAccountingCodeId(?int $accountingCodeId): self
    {
        $this->accountingCodeId = $accountingCodeId;

        return $this;
    }

    public function getAccountingPurchaseCodeId(): ?int
    {
        return $this->accountingPurchaseCodeId;
    }

    public function setAccountingPurchaseCodeId(?int $accountingPurchaseCodeId): self
    {
        $this->accountingPurchaseCodeId = $accountingPurchaseCodeId;

        return $this;
    }

    public function getIsArchived(): bool
    {
        return $this->isArchived;
    }

    public function setIsArchived(bool $isArchived): self
    {
        $this->isArchived = $isArchived;

        return $this;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    public function setCreated(\DateTime $created): self
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return array<\DonnezOrg\SellsyClient\Entity\Enum\MarketingCampaignSubscription>
     *
     * @noinspection PhpUnnecessaryFullyQualifiedNameInspection
     *
     * @psalm-suppress RedundantPropertyInitializationCheck
     */
    public function getMarketingCampaignsSubscriptions(): array
    {
        return $this->marketingCampaignsSubscriptions ?? [];
    }

    /**
     * @param array<\DonnezOrg\SellsyClient\Entity\Enum\MarketingCampaignSubscription> $marketingCampaignsSubscriptions
     *
     * @noinspection PhpUnnecessaryFullyQualifiedNameInspection
     */
    public function setMarketingCampaignsSubscriptions(array $marketingCampaignsSubscriptions): self
    {
        $this->marketingCampaignsSubscriptions = $marketingCampaignsSubscriptions;

        return $this;
    }

    /**
     * @return $this
     *
     * @psalm-suppress RedundantPropertyInitializationCheck
     */
    public function addMarketingCampaignsSubscription(MarketingCampaignSubscription $subscription): self
    {
        if (!isset($this->marketingCampaignsSubscriptions)) {
            $this->marketingCampaignsSubscriptions = [];
        }

        $this->marketingCampaignsSubscriptions[] = $subscription;

        return $this;
    }

    /**
     * @return $this
     *
     * @psalm-suppress RedundantPropertyInitializationCheck
     */
    public function removeMarketingCampaignsSubscription(MarketingCampaignSubscription $subscription): self
    {
        if (!isset($this->marketingCampaignsSubscriptions)) {
            return $this;
        }

        $index = array_search($subscription, $this->marketingCampaignsSubscriptions);
        if (false !== $index) {
            array_splice($this->marketingCampaignsSubscriptions, $index, 1);
        }

        return $this;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTime $updatedAt): Company
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getNumberOfEmployees(): ?EmployeeInfo
    {
        return $this->numberOfEmployees;
    }

    public function setNumberOfEmployees(?EmployeeInfo $numberOfEmployees): Company
    {
        $this->numberOfEmployees = $numberOfEmployees;

        return $this;
    }

    public function getBusinessSegment(): ?BusinessSegmentInfo
    {
        return $this->businessSegment;
    }

    public function setBusinessSegment(?BusinessSegmentInfo $businessSegment): Company
    {
        $this->businessSegment = $businessSegment;

        return $this;
    }

    public function getOwner(): ?Owner
    {
        return $this->owner;
    }

    public function setOwner(?Owner $owner): Company
    {
        $this->owner = $owner;

        return $this;
    }

    public function getMainContactId(): ?int
    {
        return $this->mainContactId;
    }

    public function setMainContactId(?int $mainContactId): Company
    {
        $this->mainContactId = $mainContactId;

        return $this;
    }

    public function getDunningContactId(): ?int
    {
        return $this->dunningContactId;
    }

    public function setDunningContactId(?int $dunningContactId): Company
    {
        $this->dunningContactId = $dunningContactId;

        return $this;
    }

    public function getInvoicingContactId(): ?int
    {
        return $this->invoicingContactId;
    }

    public function setInvoicingContactId(?int $invoicingContactId): Company
    {
        $this->invoicingContactId = $invoicingContactId;

        return $this;
    }

    public function getInvoicingAddressId(): ?int
    {
        return $this->invoicingAddressId;
    }

    public function setInvoicingAddressId(?int $invoicingAddressId): Company
    {
        $this->invoicingAddressId = $invoicingAddressId;

        return $this;
    }

    public function getDeliveryAddressId(): ?int
    {
        return $this->deliveryAddressId;
    }

    public function setDeliveryAddressId(?int $deliveryAddressId): Company
    {
        $this->deliveryAddressId = $deliveryAddressId;

        return $this;
    }
}
