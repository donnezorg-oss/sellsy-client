<?php

namespace DonnezOrg\SellsyClient\Entity\Company;

use DonnezOrg\SellsyClient\Entity\{
    Enum\CompanyType,
    LegalFrance,
    Owner,
    Social
};
use Symfony\Component\Validator\Constraints as Assert;

abstract class AbstractCompany
{
    protected int $id;

    /**
     * @var \DonnezOrg\SellsyClient\Entity\Enum\CompanyType
     *
     * @noinspection PhpUnnecessaryFullyQualifiedNameInspection
     */
    protected CompanyType $type;

    protected string $name;

    #[Assert\Email(mode: 'html5')]
    protected ?string $email;

    #[Assert\Url]
    protected ?string $website;

    protected ?string $phoneNumber;
    protected ?string $mobileNumber;
    protected ?string $faxNumber;
    protected ?LegalFrance $legalFrance;
    protected ?string $capital;
    protected ?string $reference;
    protected string $note;
    protected ?string $auxiliaryCode;
    protected ?Social $social;
    protected ?int $rateCategoryId;
    protected ?int $accountingCodeId;
    protected ?int $accountingPurchaseCodeId;
    protected bool $isArchived;
    protected \DateTime $created;
    protected ?\DateTime $updatedAt;
    protected ?Owner $owner;
    protected ?int $mainContactId;
    protected ?int $dunningContactId;
    protected ?int $invoicingContactId;
    protected ?int $invoicingAddressId;
    protected ?int $deliveryAddressId;

    /**
     * @var array<\DonnezOrg\SellsyClient\Entity\Enum\MarketingCampaignSubscription>
     *
     * @noinspection PhpFullyQualifiedNameUsageInspection
     */
    protected array $marketingCampaignsSubscriptions;

    public function __construct()
    {
    }
}
