<?php

namespace DonnezOrg\SellsyClient\Entity;

class Token
{
    private string $tokenType;
    private int $expiresIn;
    private string $accessToken;

    public function getTokenType(): string
    {
        return $this->tokenType;
    }

    public function setTokenType(string $tokenType): Token
    {
        $this->tokenType = $tokenType;

        return $this;
    }

    public function getExpiresIn(): int
    {
        return $this->expiresIn;
    }

    public function setExpiresIn(int $expiresIn): Token
    {
        $this->expiresIn = $expiresIn;

        return $this;
    }

    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    public function setAccessToken(string $accessToken): Token
    {
        $this->accessToken = $accessToken;

        return $this;
    }
}
