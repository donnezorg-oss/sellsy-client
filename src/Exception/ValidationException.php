<?php

namespace DonnezOrg\SellsyClient\Exception;

use Symfony\Component\Validator\ConstraintViolationListInterface;

class ValidationException extends \Exception
{
    public function __construct(ConstraintViolationListInterface $violationList)
    {
        $message = array_reduce(iterator_to_array($violationList), function ($acc, $curr) {
            return $acc.$curr.PHP_EOL;
        }, 'Validation failed:'.PHP_EOL.PHP_EOL);

        parent::__construct($message);
    }
}
