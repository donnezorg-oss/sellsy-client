<?php

namespace DonnezOrg\SellsyClient\Core;

enum HttpMethod
{
    case GET;
    case POST;
    case PUT;
    case DELETE;
    case PATCH;
}
