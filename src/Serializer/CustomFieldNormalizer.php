<?php

namespace DonnezOrg\SellsyClient\Serializer;

use DonnezOrg\SellsyClient\Entity\{
    CustomField\AbstractCustomField,
    CustomField\AmountWithCurrencyCustomField,
    CustomField\BooleanCustomField,
    CustomField\NumericCustomField,
    CustomField\NumericWithUnitCustomField,
    CustomField\RichTextCustomField,
    CustomField\SimpleTextCustomField,
    CustomField\URLCustomField,
    CustomField\UnhandledCustomField
};
use Psr\Log\LoggerInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\{DenormalizerAwareInterface, DenormalizerInterface};

class CustomFieldNormalizer implements DenormalizerInterface, DenormalizerAwareInterface
{
    private DenormalizerInterface $denormalizer;

    private const CLASS_MAPPER = [
        'simple-text' => SimpleTextCustomField::class,
        'rich-text' => RichTextCustomField::class,
        'boolean' => BooleanCustomField::class,
        'url' => URLCustomField::class,
        'numeric-with-unit' => NumericWithUnitCustomField::class,
        'numeric' => NumericCustomField::class,
        'amount' => AmountWithCurrencyCustomField::class,
    ];

    public function __construct(private readonly LoggerInterface $logger)
    {
    }

    /**
     * @throws ExceptionInterface
     * @throws \Exception
     */
    public function denormalize(mixed $data, string $type, string $format = null, array $context = []): AbstractCustomField
    {
        $type = $data['type'];
        $mappedClass = self::CLASS_MAPPER[$type] ?? null;

        return match (is_null($mappedClass)) {
            false => $this->denormalizer->denormalize($data, $mappedClass, $format, $context),
            default => $this->unhandledType($data['id'], $data['name'], $type)
        };
    }

    public function supportsDenormalization(mixed $data, string $type, string $format = null): bool
    {
        return AbstractCustomField::class === $type;
    }

    public function setDenormalizer(DenormalizerInterface $denormalizer): void
    {
        $this->denormalizer = $denormalizer;
    }

    private function unhandledType(int $id, string $name, string $type): UnhandledCustomField
    {
        $this->logger->warning(sprintf('Custom field of type %s not handled for id %d and name %s', $type, $id, $name));

        return new UnhandledCustomField($id, $name, $type);
    }
}
