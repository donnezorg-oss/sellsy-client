<?php

namespace DonnezOrg\SellsyClient\Endpoint;

use DonnezOrg\SellsyClient\Core\{Client, HttpMethod};
use DonnezOrg\SellsyClient\Entity\CollectionResult;
use DonnezOrg\SellsyClient\Entity\CustomField\{AbstractCustomField, CustomFieldMutator};
use DonnezOrg\SellsyClient\Entity\Opportunity\{Opportunity,
    OpportunityCreator,
    OpportunityMutator,
    OpportunityRankAndStepMutator,
    SearchFilter
};
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class Opportunities extends AbstractEndpoint
{
    public function __construct(
        Client $client,
        HttpClient $httpClient,
        SerializerInterface $serializer,
        ValidatorInterface $validator
    ) {
        parent::__construct(
            'opportunities',
            Opportunity::class,
            $client,
            $httpClient,
            $serializer,
            $validator
        );
    }

    /**
     * @throws GuzzleException
     */
    public function get(int $id): Opportunity
    {
        return $this->_get($id);
    }

    /**
     * Search for opportunities based on filters.
     *
     * @return CollectionResult<Opportunity>
     *
     * @throws GuzzleException
     */
    public function search(SearchFilter $filter): CollectionResult
    {
        return $this->request(
            HttpMethod::POST,
            "$this->path/search",
            ['data' => ['filters' => $filter], 'responseType' => CollectionResult::class.'<'.$this->responseType.'>']
        );
    }

    /**
     * Get a collection² of companies.
     *
     * @return CollectionResult<Opportunity>
     *
     * @throws GuzzleException
     */
    public function getAll(): CollectionResult
    {
        return $this->_getAll();
    }

    /**
     * @throws GuzzleException
     */
    public function update(int $id, OpportunityMutator $mutator): Opportunity
    {
        return $this->_update($id, $mutator);
    }

    /**
     * @throws GuzzleException
     */
    public function create(OpportunityCreator $creator): Opportunity
    {
        return $this->_create($creator);
    }

    /**
     * Update the rank and/or the step of an opportunity.
     *
     * @param int $opportunityId the opportunity id
     *
     * @throws GuzzleException
     */
    public function updateRankAndStep(int $opportunityId, OpportunityRankAndStepMutator $mutator): void
    {
        $this->request(
            HttpMethod::PATCH,
            "$this->path/$opportunityId/step-rank",
            ['data' => $mutator]
        );
    }

    /**
     * @param int                  $opportunityId the opportunity id
     * @param CustomFieldMutator[] $mutator
     *
     * @throws GuzzleException
     */
    public function updateCustomFields(int $opportunityId, array $mutator): void
    {
        $this->request(
            HttpMethod::PUT,
            "$this->path/$opportunityId/custom-fields",
            ['data' => $mutator]
        );
    }

    /**
     * @param int $opportunityId the opportunity id
     *
     * @return CollectionResult<AbstractCustomField>
     *
     * @throws GuzzleException
     */
    public function getCustomFields(int $opportunityId): CollectionResult
    {
        return $this->request(
            HttpMethod::GET,
            "$this->path/$opportunityId/custom-fields",
            ['responseType' => CollectionResult::class.'<'.AbstractCustomField::class.'>']
        );
    }
}
