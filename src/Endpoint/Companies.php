<?php

namespace DonnezOrg\SellsyClient\Endpoint;

use DonnezOrg\SellsyClient\Core\{Client, HttpMethod};
use DonnezOrg\SellsyClient\Entity\Address\{Address, AddressCreator, AddressMutator};
use DonnezOrg\SellsyClient\Entity\CollectionResult;
use DonnezOrg\SellsyClient\Entity\Company\{Company, CompanyCreator, CompanyMutator};
use DonnezOrg\SellsyClient\Entity\Contact\ContactLinkCreator;
use DonnezOrg\SellsyClient\Entity\CustomField\CustomFieldMutator;
use DonnezOrg\SellsyClient\Entity\SmartTag\{SmartTag, SmartTagLinkByIdentifierCreator, SmartTagLinkByValueCreator};
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class Companies extends AbstractEndpoint
{
    public function __construct(
        Client $client,
        HttpClient $httpClient,
        SerializerInterface $serializer,
        ValidatorInterface $validator
    ) {
        parent::__construct(
            'companies',
            Company::class,
            $client,
            $httpClient,
            $serializer,
            $validator
        );
    }

    /**
     * @throws GuzzleException
     */
    public function get(int $id): Company
    {
        return $this->_get($id);
    }

    /**
     * Get a collection² of companies.
     *
     * @return CollectionResult<Company>
     *
     * @throws GuzzleException
     */
    public function getAll(): CollectionResult
    {
        return $this->_getAll();
    }

    /**
     * @throws GuzzleException
     */
    public function update(int $id, CompanyMutator $mutator): Company
    {
        return $this->_update($id, $mutator);
    }

    /**
     * @throws GuzzleException
     */
    public function create(CompanyCreator $creator): Company
    {
        return $this->_create($creator);
    }

    /**
     * @throws GuzzleException
     */
    public function getAddress(int $companyId, int $addressId): Address
    {
        return $this->request(
            HttpMethod::GET,
            "$this->path/$companyId/address/$addressId",
            ['responseType' => Address::class]
        );
    }

    /**
     * @param int $companyId the company it for which to get addresses
     *
     * @return CollectionResult<Address>
     *
     * @throws GuzzleException
     */
    public function getAddresses(int $companyId): CollectionResult
    {
        return $this->request(
            HttpMethod::GET,
            "$this->path/$companyId/addresses",
            ['responseType' => CollectionResult::class.'<'.Address::class.'>']
        );
    }

    /**
     * @throws GuzzleException
     */
    public function createAddress(int $companyId, AddressCreator $creator): Address
    {
        return $this->request(
            HttpMethod::POST,
            "$this->path/$companyId/addresses",
            ['data' => $creator, 'responseType' => Address::class]
        );
    }

    /**
     * @throws GuzzleException
     */
    public function updateAddress(int $companyId, int $addressId, AddressMutator $mutator): Address
    {
        return $this->request(
            HttpMethod::PUT,
            "$this->path/$companyId/addresses/$addressId",
            ['data' => $mutator, 'responseType' => Address::class]
        );
    }

    /**
     * @throws GuzzleException
     */
    public function deleteAddress(int $companyId, int $addressId): void
    {
        $this->request(HttpMethod::DELETE, "$this->path/$companyId/addresses/$addressId");
    }

    /**
     * @param int $companyId the company id to link the contact to
     * @param int $contactId the contact id to link
     *
     * @throws GuzzleException
     */
    public function createContactLink(int $companyId, int $contactId, ?ContactLinkCreator $creator = null): void
    {
        $this->request(
            HttpMethod::POST,
            "$this->path/$companyId/contacts/$contactId",
            ['data' => $creator ?? new ContactLinkCreator()]
        );
    }

    /**
     * @param int                  $companyId the company id
     * @param CustomFieldMutator[] $mutator
     *
     * @throws GuzzleException
     */
    public function updateCustomFields(int $companyId, array $mutator): void
    {
        $this->request(
            HttpMethod::PUT,
            "$this->path/$companyId/custom-fields",
            ['data' => $mutator, 'responseType' => null]
        );
    }

    /**
     * @param int $companyId the company id to link the smart tags to
     *
     * @return CollectionResult<SmartTag>
     *
     * @throws GuzzleException
     */
    public function linkSmartTags(int $companyId, SmartTagLinkByIdentifierCreator|SmartTagLinkByValueCreator ...$creators): CollectionResult
    {
        return $this->request(
            HttpMethod::POST,
            "$this->path/$companyId/smart-tags",
            ['data' => [...$creators], 'responseType' => CollectionResult::class.'<'.SmartTag::class.'>']
        );
    }
}
