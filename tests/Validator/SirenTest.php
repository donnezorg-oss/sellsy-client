<?php

namespace DonnezOrg\SellsyClient\Tests\Validator;

use DonnezOrg\SellsyClient\Validator\Siren;

final class SirenTest extends ValidatorTestCase
{
    public function testInvalidFormat(): void
    {
        $result = $this->validator->validate(new SirenDto('530 514157'));
        $this->assertCount(1, $result);
        $this->assertEquals('Value should be a numeric string', $result[0]->getMessage());
    }

    public function testInvalidLength(): void
    {
        $result = $this->validator->validate(new SirenDto('5305141'));
        $this->assertCount(1, $result);
        $this->assertEquals('Value length should be equal to 9 characters', $result[0]->getMessage());
    }

    public function testNull(): void
    {
        $result = $this->validator->validate(new SirenDto(null));
        $this->assertCount(0, $result);
    }

    public function testValid(): void
    {
        $result = $this->validator->validate(new SirenDto('530514157'));
        $this->assertCount(0, $result);
    }
}

class SirenDto
{
    public function __construct(
        #[Siren]
        public ?string $siren
    ) {
    }
}
