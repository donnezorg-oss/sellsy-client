<?php

namespace DonnezOrg\SellsyClient\Tests\Endpoint;

use DonnezOrg\SellsyClient\Entity\Contact\ContactCreator;
use DonnezOrg\SellsyClient\Entity\CustomField\CustomFieldMutator;
use DonnezOrg\SellsyClient\Entity\Enum\MarketingCampaignSubscription;
use GuzzleHttp\Exception\GuzzleException;

final class ContactTest extends EndpointTestCase
{
    /**
     * @throws GuzzleException
     */
    public function testContactsGet(): void
    {
        $this->addMockedResponse('contacts/get_one.json');
        $contact = $this->client->contacts()->get(6657);
        $this->assertEquals('Doe', $contact->getLastName());
        $this->assertEquals('John', $contact->getFirstName());
        $this->assertContains(MarketingCampaignSubscription::SMS, $contact->getMarketingCampaignsSubscriptions());
    }

    /**
     * @throws GuzzleException
     */
    public function testContactsGetAll(): void
    {
        $this->addMockedResponse('contacts/get_all.json');
        $result = $this->client->contacts()->getAll();
        $pagination = $result->getPagination();

        $this->assertEquals(1, $pagination->getCount());
        $this->assertEquals(25, $pagination->getLimit());
        $this->assertEquals(25, $pagination->getOffset());
        $this->assertEquals(152, $pagination->getTotal());
        $this->assertCount(1, $result->getData());

        $firstResult = $result->getData()[0];
        $this->assertEquals('Doe', $firstResult->getLastName());
        $this->assertEquals('John', $firstResult->getFirstName());
        $this->assertContains(MarketingCampaignSubscription::SMS, $firstResult->getMarketingCampaignsSubscriptions());
    }

    /**
     * @throws GuzzleException
     */
    public function testContactCreate(): void
    {
        $this->addMockedResponse('contacts/create.json');
        $contact = $this->client->contacts()->create(
            (new ContactCreator('PHPUnit contact'))
                ->addMarketingCampaignsSubscription(MarketingCampaignSubscription::EMAIL)
                ->addMarketingCampaignsSubscription(MarketingCampaignSubscription::PHONE)
        );
        $this->assertEquals('Doe', $contact->getLastName());
        $this->assertEquals('John', $contact->getFirstName());
        $this->assertContains(MarketingCampaignSubscription::SMS, $contact->getMarketingCampaignsSubscriptions());
    }

    /**
     * @throws GuzzleException
     */
    public function testContactsCustomFieldsUpdate(): void
    {
        $this->addMockedResponse();
        $this->client->contacts()->updateCustomFields(
            6657,
            [new CustomFieldMutator(
                1,
                2
            )]
        );
        $this->assertTrue(true);
    }
}
