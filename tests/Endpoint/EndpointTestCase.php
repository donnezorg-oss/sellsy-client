<?php

namespace DonnezOrg\SellsyClient\Tests\Endpoint;

use DonnezOrg\SellsyClient\Core\{Client, StdLogger};
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\{Client as HttpClient, HandlerStack};
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;

class EndpointTestCase extends TestCase
{
    protected Client $client;
    protected MockHandler $handler;

    /** @var LoggerInterface&MockObject */
    protected MockObject $logger;

    protected function setUp(): void
    {
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->handler = new MockHandler();
        $handlerStack = HandlerStack::create($this->handler);
        // TODO: Add guzzle middleware to compute post body on response
        //        $handlerStack->push(new ResponseTransformer());
        $httpClient = new HttpClient(['handler' => $handlerStack]);

        $this->client = new Client(
            '',
            '',
            $this->logger,
            $httpClient
        );
    }

    protected function addMockedResponse(?string $responseFileName = null, int $code = 200, array $headers = []): void
    {
        $filePath = __DIR__.'/responses/'.$responseFileName;
        $responseContent = null;
        if (isset($responseFileName)) {
            if (!file_exists($filePath)) {
                throw new FileNotFoundException("File $filePath not found");
            }
            $responseContent = file_get_contents($filePath);
        }

        $this->handler->append(new Response($code, $headers, $responseContent));
    }
}
